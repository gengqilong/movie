import urllib

import cv2
import os

import simplejson

from movie.settings import fileLocation


def verifyShould(request,parameter,required):
    parameter=request.POST.get(parameter,"")
    if (required==True and parameter=="" ):
        return "no parameter"
    else:
        return parameter


def upload_movie(file):
    movielocation=os.path.join(fileLocation, file.name)
    with open(movielocation, 'wb+') as destination:#上传视频
        for chunk in file.chunks():
            destination.write(chunk)

    cap = cv2.VideoCapture(movielocation)
    ret, frame = cap.read()
    if ret:
        picName=file.name.split(".")[0] + ".jpg"
        piclocation = os.path.join(fileLocation,file.name.split(".")[0] + ".jpg")
        cv2.imencode('.jpg', frame)[1].tofile(piclocation)
    return file.name,movielocation,picName,piclocation

def upload_pic(file):
    piclocation=os.path.join(fileLocation, file.name)
    with open(piclocation, 'wb+') as destination:#上传视频
        for chunk in file.chunks():
            destination.write(chunk)
    return file.name,piclocation

def down_file(fileLocation):
    with open(fileLocation, 'rb') as src:#下载视频
        file_data=src.read()
    return file_data

def decode(input):
    if input:
        return urllib.parse.unquote(input)
    else:
        return input

def encode(input):
    if input:
        return urllib.parse.quote(input)
    else:
        return input