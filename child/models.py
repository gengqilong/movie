from django.db import models

# Create your models here.
class Child(models.Model):
    class Meta:
        db_table="child"
    childId=models.AutoField(primary_key=True)
    childPicName = models.CharField(max_length=128,null=True,default="")
    childPicLocation = models.CharField(max_length=128, null=True, default="")
    childBirthDay = models.DateTimeField(null=True)
    childSex = models.CharField(max_length=2) #0为男，1为女
    childName=models.CharField(max_length=32)
    creat_time=models.DateTimeField(auto_now_add=True)
    update_time=models.DateTimeField(auto_now=True)

    def __repr__(self):
        return "".format(self.childId, self.childName)

    __str__ = __repr__
