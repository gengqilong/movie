# Generated by Django 2.2.1 on 2019-05-15 17:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Child',
            fields=[
                ('childId', models.AutoField(primary_key=True, serialize=False)),
                ('childPicName', models.CharField(default='', max_length=128, null=True)),
                ('childPicLocation', models.CharField(default='', max_length=128, null=True)),
                ('childBirthDay', models.DateTimeField(null=True)),
                ('childName', models.CharField(max_length=32)),
                ('creat_time', models.DateTimeField(auto_now_add=True)),
                ('update_time', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'child',
            },
        ),
    ]
