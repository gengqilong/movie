from math import ceil

from django.core.paginator import Paginator
from django.db.models import Q
from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse
from child.models import Child
from movie.settings import childUrlBase
from movie.views import verifyShould, upload_pic, decode, down_file
from movies.movies_views import getChildBirth


def verifyAddChild(request):#新增必填校验
    child = Child()
    child.PicFile = verifyShould(request, 'PicFile', True)
    child.childBirthDay = verifyShould(request, 'childBirthDay', True)
    child.childSex = verifyShould(request, 'childSex', True)
    child.childName = decode(verifyShould(request, 'childName', True))
    return child

def verifyUpdateChild(request):#更新必填校验
    child = Child()
    child.childId = verifyShould(request, 'childId', True)
    # child.PicFile = verifyShould(request, 'PicFile', True)
    child.childBirthDay = verifyShould(request, 'childBirthDay', True)
    child.childSex = verifyShould(request, 'childSex', True)
    child.childName = verifyShould(request, 'childName', True)
    return child

#获取movieList
def returnChildList(quest):
    childlist=[]
    for child in quest:
        mychild = model_to_dict(child,exclude=["childPicLocation"])
        mychild["picUrl"] = childUrlBase + str(mychild["childId"])
        date=getChildBirth(mychild["childId"])
        mychild["childBirthDay"] = date
        childlist.append(mychild)
    return childlist

def childadd(request:HttpResponse):
    if request.method=="POST":
        message=""
        child = verifyAddChild(request)#验证接口必填项
        ChildDict = model_to_dict(child, exclude="childId")
        myFile = request.FILES.get("PicFile", None)  # 获取文件
        if "no parameter" in ChildDict.values():
            message = "缺少参数"
        else:
            if myFile:
                child.childPicName, child.childPicLocation = upload_pic(myFile)
            try:
                child.save()
                data = model_to_dict(child)
            except Exception as e:
                print(e)
                return JsonResponse(data={"data": "保存失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def childupdate(request:HttpResponse):
    if request.method=="POST":
        message=""
        changeImage=request.POST.get("changeImage","false")
        child = verifyUpdateChild(request)
        ChildDict = model_to_dict(child)
        myFile = request.FILES.get("PicFile", None)  # 获取文件

        if(changeImage=="false"):
            pass
        else:
            changeImage="true"

        if ("no parameter" in ChildDict.values()) or (changeImage=="true" and myFile==None ):
            message = "缺少参数"
        else:
            if myFile:
                child.childPicName, child.childPicLocation = upload_pic(myFile)
            try:
                quest = Child.objects.filter(Q(childId__exact=child.childId))#查询是否有这个数据
                if quest:
                    if changeImage=="true":#图片有变更
                        mychild=model_to_dict(child,exclude=["childId", "creat_time", "update_time"])
                    else:
                        child.childPicName=quest.first().childPicName
                        child.childPicLocation = quest.first().childPicLocation
                        mychild = model_to_dict(child, exclude=["childId", "creat_time", "update_time"])
                    quest.update(**mychild)
                    data = mychild
                else:
                    message="没有这个child记录"
            except Exception as e:
                return JsonResponse(data={"data": "更新失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def childGetAll(request:HttpResponse):
    if request.method=="GET":
        message=""
        pageNum = request.GET.get("pageNum", 1)
        try:
            quest = Child.objects.filter().all().order_by("-creat_time")#查询是否有视频
            if not quest:
                message = "没有任何记录"
            else:
                paginator = Paginator(quest, 4)#搜索结果分页
                totalNum=ceil(len(quest)/4)
                moviePaginator = paginator.page(pageNum)#查询页数
                data=returnChildList(moviePaginator)
        except Exception as e:
                return JsonResponse(data={"data": "查询失败或没有结果"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data ,"totalNum":totalNum})

def getChildPic(request:HttpResponse):
    if request.method=="GET":
        childId=request.GET.get("childId","")
        if not childId:
            message = "缺少参数"
        else:
            try:
                quest = Child.objects.filter(childId=childId).first()#查询是否有这个数据
                if not quest:
                    message = "没有任何记录"
                else:
                    jpg=down_file(model_to_dict(quest)["childPicLocation"])
                    return HttpResponse(jpg, content_type="image/jpeg")
            except Exception as e:
                message="查询失败"
    else:
        message= "method错误"
    return JsonResponse(data={"status": "ok", "message": message})

def childGetOne(request:HttpResponse):
    if request.method=="GET":
        message=""
        childId = request.GET.get("childId")
        if childId:
            try:
                quest = Child.objects.filter(childId=childId).all()
                if not quest:
                    message = "没有任何记录"
                else:
                    data=returnChildList(quest)
            except Exception as e:
                    return JsonResponse(data={"data": "查询失败或没有结果"})
        else:
            message = "缺少参数"
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data} )
