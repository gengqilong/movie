from django.conf.urls import url

from child.views import *

urlpatterns = [
    url(r'^add', childadd),
    url(r'^update', childupdate),
    url(r'^getChildPic', getChildPic),
    url(r'^getAll', childGetAll),
    url(r'^getOne', childGetOne),
]