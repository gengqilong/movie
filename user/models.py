
# Create your models here.
from django.db import models

# Create your models here.
class User(models.Model):
    class Meta:
        db_table="user"
    userId=models.AutoField(primary_key=True)
    userCode = models.CharField(max_length=64, null=False)
    userName=models.CharField(max_length=16,null=False)
    childId = models.IntegerField()
    relation=models.CharField(max_length=16,null=True)
    creat_time=models.DateTimeField(auto_now_add=True)
    update_time=models.DateTimeField(auto_now=True)
    del_flag=models.BooleanField()
    def __repr__(self):
        return "".format(self.userId, self.userName)

class Userfavour(models.Model):
    class Meta:
        db_table="userfavour"
    id = models.AutoField(primary_key=True)
    userCode = models.CharField(max_length=64, null=False)
    userName = models.CharField(max_length=16, null=False)
    childId = models.IntegerField()
    movieId = models.IntegerField()
    creat_time=models.DateTimeField(auto_now_add=True)
    update_time=models.DateTimeField(auto_now=True)
    del_flag=models.BooleanField()
    def __repr__(self):
        return "".format(self.userId, self.userName)
