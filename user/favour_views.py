from django.core.paginator import Paginator
from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse
# Create your views here.
from movie.views import verifyShould, decode
from movies.models import Movies
from movies.movies_views import returnMovieList
from user.models import  Userfavour

def verifyAddFavour(request):#新增必填校验
    userfavour = Userfavour()
    userfavour.userName = decode(verifyShould(request, 'userName', True))
    userfavour.userCode = verifyShould(request, 'userCode', False)  # 必填校验
    userfavour.childId = verifyShould(request, 'childId', True)
    userfavour.movieId = verifyShould(request, 'movieId', True)
    return userfavour

def verifyDeleteFavour(request):#更新必填校验
    userfavour = Userfavour()
    userfavour.userName = decode(verifyShould(request, 'userName', True))
    userfavour.userCode = verifyShould(request, 'userCode', False)  # 必填校验
    userfavour.childId = verifyShould(request, 'childId', True)
    userfavour.movieId = verifyShould(request, 'movieId', True)
    return userfavour

def verifyJudgeFavour(request):#更新必填校验
    userfavour = Userfavour()
    userfavour.userName = decode(verifyShould(request, 'userName', True))
    userfavour.userCode = verifyShould(request, 'userCode', False)  # 必填校验
    userfavour.movieId = verifyShould(request, 'movieId', True)
    userfavour.childId = verifyShould(request, 'childId', True)
    return userfavour

def userfavourAdd(request:HttpResponse):
    if request.method=="POST":
        message =""
        favour=verifyAddFavour(request)
        if ("no parameter" in model_to_dict(favour).values()):
            message = "缺少参数"
        else:
            try:
                favour.del_flag = False
                favour.save()
                data=model_to_dict(favour)
            except Exception as e:
                print(e)
                return JsonResponse(data={"data": "保存失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def userfavourDelete(request:HttpResponse):
    if request.method=="POST":
        data=""
        message =""
        favour=verifyDeleteFavour(request)
        if ("no parameter" in model_to_dict(favour).values()):
            message = "缺少参数"
        else:
            try:
                quest = Userfavour.objects.filter(userCode=favour.userCode,movieId=favour.movieId,childId=favour.childId, del_flag=False)
                if quest:
                    data="取消成功"
                    quest.update(del_flag=True)
            except Exception as e:
                print(e)
                return JsonResponse(data={"data": "保存失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def userfavourJudge(request:HttpResponse):
    if request.method=="POST":
        data=""
        message =""
        judge=""
        favour=verifyJudgeFavour(request)
        if ("no parameter" in model_to_dict(favour).values()):
            message = "缺少参数"
        else:
            try:
                quest = Userfavour.objects.filter(userCode=favour.userCode,movieId=favour.movieId,childId=favour.childId, del_flag=False)
                if quest:
                    judge=True
                else:
                    judge=False
            except Exception as e:
                return JsonResponse(data={"message": "保存失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": judge})

def userfavourGet(request:HttpResponse):
    if request.method=="GET":
        message=""
        data = []
        userCode=decode(request.GET.get("userCode",""))
        childId = request.GET.get("childId", "")
        pageNum=request.GET.get("pageNum", 1)
        if not userCode and not childId:
            message = "缺少参数"
        else:
            try:
                pass
                movieList = Userfavour.objects.filter(userCode=userCode, childId=childId, del_flag=False).values_list("movieId",flat=True).order_by("-creat_time")#查询是否有这个数据
                if not movieList:#movieList为queryset类型
                    message = "没有任何记录"
                else:
                    movieQuest=Movies.objects.filter(movieId__in=movieList,del_flag=False).order_by("-creat_time")
                    paginator = Paginator(movieQuest, 2)  # 搜索结果分页
                    moviePaginator = paginator.page(pageNum)  # 查询页数
                    data=returnMovieList(moviePaginator)
            except Exception as e:
                print(e)
                return JsonResponse(data={"message": "查询失败或没有结果"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})
