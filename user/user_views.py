from math import ceil

from django.core.paginator import Paginator
from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse
# Create your views here.
import requests

from child.models import Child
from child.views import returnChildList
from movie.settings import appid, secret, grant_type, weixinUrl
from movie.views import verifyShould, encode, decode
from user.models import User


def verifyAddUser(request):#新增必填校验
    user = User()
    user.userName = decode(verifyShould(request, 'userName', True))
    user.userCode = verifyShould(request, 'userCode', True)
    user.childId = verifyShould(request, 'childId', True)
    user.relation = decode(verifyShould(request, 'relation', False))
    return user

def verifyUpdateUser(request):#更新必填校验
    user = User()
    user.userName = verifyShould(request, 'userName', True)
    user.userCode = verifyShould(request, 'userCode', True)
    user.childId = verifyShould(request, 'childId', True)
    user.relation = verifyShould(request, 'relation', False)
    return user

def verifyDeleteUser(request):#更新必填校验
    user = User()
    user.userId = verifyShould(request, 'userId', True)
    return user

def userAdd(request:HttpResponse):
    if request.method=="POST":
        message =""
        user=verifyAddUser(request)
        if ("no parameter" in model_to_dict(user).values()):
            message = "缺少参数"
        else:
            try:
                quest = User.objects.filter(userCode=user.userCode,childId=user.childId,del_flag=False).first()  # 查询是否有这个数据
                if quest:
                    message = "已存在数据"
                else:
                    user.del_flag=False
                    user.save()
                    data=model_to_dict(user)
            except Exception as e:
                print(e)
                return JsonResponse(data={"data": "保存失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})
def userUpdate(request:HttpResponse):
    if request.method=="POST":
        message=""
        message = ""
        user = verifyUpdateUser(request)
        if ("no parameter" in model_to_dict(user).values()):
            message = "缺少参数"
        else:
            try:
                quest = User.objects.filter(userCode=user.userCode,childId=user.childId,del_flag=False)  # 查询是否有这个数据
                if not quest:
                    user.del_flag = False
                    user.save()
                    data = model_to_dict(user)
                else:
                    user.del_flag=False
                    myuser = model_to_dict(user, exclude=["userId", "creat_time", "update_time"])
                    quest.update(**myuser)
                    data = myuser
            except Exception as e:
                print(e)
                return JsonResponse(data={"data": "保存失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def userDelete(request:HttpResponse):
    pass

def userLogin(request:HttpResponse):
    if request.method=="GET":
        message = ""
        code = request.GET.get("code", "")
        if not code:
            message = "缺少参数"
        else:
            params={"appid": appid, "secret": secret,"grant_type":grant_type,"js_code":code}
            try:
                response=requests.get(url=weixinUrl,params=params,verify=False)
                response.encoding= 'utf-8'
                datas=response.json()
                print(datas)
                if(datas["openid"]):
                    data=datas["openid"]
                else:
                    message="查询失败或没有结果"
            except Exception as e:
                return JsonResponse(data={"message": "查询失败或没有结果"})
    else:
        message = "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def favourchild(request:HttpResponse):
    if request.method=="GET":
        message=""
        pageNum = request.GET.get("pageNum", 1)
        userCode = request.GET.get("userCode", "")
        if not userCode:
            message = "缺少参数"
        else:
            try:
                quest = User.objects.filter(userCode=userCode, del_flag=False).values_list("childId",flat=True)#查询是否有这个数据
                childQuest = Child.objects.filter(childId__in=quest).order_by("-creat_time")
                if not childQuest:
                    message = "没有任何记录"
                else:
                    paginator = Paginator(childQuest, 4)#搜索结果分页
                    totalNum=ceil(len(childQuest)/4)
                    childPaginator = paginator.page(pageNum)#查询页数
                    data=returnChildList(childPaginator)
            except Exception as e:
                print(e)
                return JsonResponse(data={"message": "查询失败或没有结果"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data ,"totalNum":totalNum})

def otherchild(request:HttpResponse):
    if request.method=="GET":
        message=""
        pageNum = request.GET.get("pageNum", 1)
        userCode = request.GET.get("userCode", "")
        if not userCode:
            message = "缺少参数"
        else:
            try:
                quest = User.objects.filter(userCode=userCode, del_flag=False).values_list("childId",flat=True)#查询是否有这个数据
                childQuest = Child.objects.exclude(childId__in=quest).order_by("-creat_time")
                if not childQuest:
                    message = "没有任何记录"
                else:
                    paginator = Paginator(childQuest, 4)#搜索结果分页
                    totalNum=ceil(len(childQuest)/4)
                    childPaginator = paginator.page(pageNum)#查询页数
                    data=returnChildList(childPaginator)
            except Exception as e:
                    return JsonResponse(data={"message": "查询失败或没有结果"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data ,"totalNum":totalNum})

def getRelation(request:HttpResponse):
    if request.method=="GET":
        message=""
        userCode = request.GET.get("userCode", "")
        childId = request.GET.get("childId", "")
        if (not userCode) and (not childId):
            message = "缺少参数"
        else:
            try:
                quest = User.objects.filter(userCode=userCode, childId=childId,del_flag=False).first()#查询是否有这个数据
                if not quest:
                    message = "没有任何记录"
                else:
                    data=model_to_dict(quest)
            except Exception as e:
                    return JsonResponse(data={"data": "查询失败或没有结果"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data })