from django.conf.urls import url

from user.favour_views import *
from user.user_views import *

urlpatterns = [
    url(r'^add', userAdd),
    url(r'^update', userUpdate),
    url(r'^delete', userDelete),
    url(r'^login', userLogin),
    url(r'^favourchild', favourchild),
    url(r'^otherchild', otherchild),
    url(r'^getRelation', getRelation),
    url(r'^movie/favouradd', userfavourAdd),
    url(r'^movie/favourdelete', userfavourDelete),
    url(r'^movie/favourget', userfavourGet),
    url(r'^movie/favourJudge', userfavourJudge),
]