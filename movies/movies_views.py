

from django.core.paginator import Paginator
from django.db.models import Q
from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse

from child.models import Child
from movie.settings import movieUrlBase, picUrlBase
from movie.views import verifyShould, upload_movie, down_file, decode
from movies.models import Movies
from user.models import  Userfavour

def verifyMovieUrl(movie):#判断movieUrl的开头
    return movie.movieUrl.startswith("http:")or movie.movieUrl.startswith("https:")

#判断是否有这个childId
def verifyChildId(movie):
    return Child.objects.filter(Q(childId__exact=movie.childId))

def getChildBirth(childId):
    return Child.objects.filter(childId=childId).first().childBirthDay.date()

#获取movieList
def returnMovieList(moviePaginator):
    movielist=[]
    for movie in moviePaginator:
        mymovie = model_to_dict(movie,exclude=["movielocation","piclocation"])
        mymovie["movieUrl"] =movieUrlBase+str(mymovie["movieId"])
        mymovie["picUrl"] = picUrlBase + str(mymovie["movieId"])
        mymovie["creat_time"]=mymovie["creat_time"].date()
        date=getChildBirth(mymovie["childId"])
        mymovie["timeDiff"]=getTimeDiff(mymovie["creat_time"]-date)
        movielist.append(mymovie)
    return movielist

#获取时间差
def getTimeDiff(days):
    result=""
    days=days.days
    year=int(days/365)
    month=int((days%365)/30)
    day=int(days%30)
    if year:
        result=result+str(year)+"年"
    if month:
        result=result+str(month)+"个月"
    if day:
        result=result+str(day)+"日"
    return result

def movieAdd(request:HttpResponse):
    if request.method=="POST":
        message=""
        movie = Movies()
        movie.relation = verifyShould(request, 'relation', True)#必填校验
        movie.childId = verifyShould(request, 'childId', True)  # 必填校验
        myFile = request.FILES.get("movieFile","")#获取文件
        if ("no parameter"== movie.childId)and (not myFile) and ("no parameter"== movie.relation):
            message = "缺少了参数"
        elif not verifyChildId(movie):
            message="没有这个childId"
        else:
            try:
                movie.movieName,movie.movielocation,movie.picName,movie.piclocation=upload_movie(myFile)#获取文件名和文件存放地址
                movie.del_flag = False  # 新增的删除flag为0
                movie.save()
                data=model_to_dict(movie)
            except Exception as e:
                print(e)
                return JsonResponse(data={"data": "保存失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def movieDelete(request:HttpResponse):
    if request.method=="POST":
        message = ""
        movieId=verifyShould(request, 'movieId', True)
        data=movieId
        if ("no parameter"==movieId) :
            message = "缺少了参数"
        else:
            try:
                questmovie = Movies.objects.filter(movieId=movieId,del_flag=False)#查询是否有这个数据
                questfavour=Userfavour.objects.filter(movieId=movieId,del_flag=False)#判断userfavour表里是否有这个MovieId
                if questmovie:
                    questmovie.update(del_flag=True)
                else:
                    message="没有这个movie记录"
                if questfavour:
                    questfavour.update(del_flag=True)
            except Exception as e:
                return JsonResponse(data={"data": "删除失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def movieGetAll(request:HttpResponse):
    if request.method=="GET":
        message=""
        pageNum=request.GET.get("pageNum",1)
        try:
            quest = Movies.objects.filter(del_flag=False).all().order_by("-creat_time")#查询是否有视频
            if not quest:
                message = "没有任何记录"
            else:
                paginator = Paginator(quest, 2)#搜索结果分页
                moviePaginator = paginator.page(pageNum)#查询页数
                data=returnMovieList(moviePaginator)
        except Exception as e:
                return JsonResponse(data={"data": "查询失败或没有结果"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def movieGetOneChild(request:HttpResponse):
    if request.method=="GET":
        message=""
        childId=request.GET.get("childId","")
        pageNum = request.GET.get("pageNum", 1)
        if not childId:
            message = "缺少了参数"
        else:
            try:
                quest = Movies.objects.filter(childId=childId,del_flag=False).all().order_by("-creat_time")#查询是否有这个数据
                if not quest:
                    message = "没有任何记录"
                else:
                    paginator = Paginator(quest, 2)  # 搜索结果分页
                    moviePaginator = paginator.page(pageNum)  # 查询页数
                    data = returnMovieList(moviePaginator)
            except Exception as e:
                    return JsonResponse(data={"message": "查询失败或没有结果"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def movieGetDetail(request:HttpResponse):
    if request.method=="GET":
        movieId=request.GET.get("movieId","")
        if not movieId:
            message = "缺少参数"
        else:
            try:
                quest = Movies.objects.filter(movieId=movieId,del_flag=False).first()#查询是否有这个数据
                if not quest:
                    message = "没有任何记录"
                else:
                    movie=down_file(model_to_dict(quest)["movielocation"])
                    return HttpResponse(movie, content_type="video/mpeg4")
            except Exception as e:
                message="查询失败"
    else:
        message= "method错误"
    return JsonResponse(data={"status": "ok", "message": message})

def picGetDetail(request:HttpResponse):
    if request.method=="GET":
        movieId=request.GET.get("movieId","")
        if not movieId:
            message = "缺少参数"
        else:
            try:
                quest = Movies.objects.filter(movieId=movieId,del_flag=False).first()#查询是否有这个数据
                if not quest:
                    message = "没有任何记录"
                else:
                    jpg=down_file(model_to_dict(quest)["piclocation"])
                    return HttpResponse(jpg, content_type="image/jpeg")
            except Exception as e:
                message="查询失败"
    else:
        message= "method错误"
    return JsonResponse(data={"status": "ok", "message": message})

