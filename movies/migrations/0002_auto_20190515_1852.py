# Generated by Django 2.2.1 on 2019-05-15 18:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='danmu',
            name='danmuId',
        ),
        migrations.AlterField(
            model_name='danmu',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
