import math

from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse

from movie.views import verifyShould, decode
from movies.models import Danmu

def verifyAddDanmu(request):#新增必填校验
    danmu = Danmu()
    danmu.movieId = verifyShould(request, 'movieId', True)  # 必填校验
    danmu.text = decode(verifyShould(request, 'text', True))  # 必填校验
    danmu.color = decode(verifyShould(request, 'color', True))  # 必填校验
    danmu.time = verifyShould(request, 'time', True)  # 必填校验
    danmu.relation = decode(verifyShould(request, 'relation', False))  # 必填校验
    danmu.userCode = verifyShould(request, 'userCode', False)  # 必填校验
    return danmu

def danmuAdd(request:HttpResponse):
    if request.method=="POST":
        message =""
        danmu=verifyAddDanmu(request)
        if ("no parameter" in model_to_dict(danmu).values()):
            message = "缺少参数"
        else:
            try:
                danmu.save()
                data=model_to_dict(danmu)
            except Exception as e:
                print(e)
                return JsonResponse(data={"data": "保存失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})


def danmuGetOneMovie(request:HttpResponse):
    if request.method=="GET":
        message=""
        data =[]
        movieId=request.GET.get("movieId","")
        if not movieId:
            message = "缺少参数"
        else:
            try:
                quest = Danmu.objects.filter(movieId=movieId).all().order_by("creat_time")#查询是否有这个数据
                if not quest:
                    message = "没有任何记录"
                else:
                    for danmu in quest:
                        myDanmu={}
                        myDanmu["text"]=danmu.relation+":"+danmu.text
                        myDanmu["color"] = danmu.color
                        time=math.ceil(float(danmu.time))
                        ceil=lambda x:x if x>0 else 1
                        myDanmu["time"] = ceil(time)
                        data.append(myDanmu)
            except Exception as e:
                    return JsonResponse(data={"message": "查询失败或没有结果"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})