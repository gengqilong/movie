# Create your models here.
from django.db import models

# Create your models here.
class Movies(models.Model):
    class Meta:
        db_table="movies"
    movieId=models.AutoField(primary_key=True)
    movielocation=models.CharField(max_length=128,null=False)
    piclocation = models.CharField(max_length=128, null=False)
    childId = models.IntegerField()
    movieName=models.CharField(max_length=128,null=False)
    picName = models.CharField(max_length=128, null=False)
    relation = models.CharField(max_length=16, null=True)
    creat_time=models.DateTimeField(auto_now_add=True)
    update_time=models.DateTimeField(auto_now=True)
    del_flag=models.BooleanField(default=0)#1删除，0正常
    def __repr__(self):
        return "".format(self.movieId, self.movieUrl)
    __str__ = __repr__

class Danmu(models.Model):
    class Meta:
        db_table="danmu"
    id = models.AutoField(primary_key=True)
    movieId = models.IntegerField()
    text=models.CharField(max_length=128,null=False)
    color = models.CharField(max_length=16, null=False)
    time = models.CharField(max_length=10,null=False)
    relation=models.CharField(max_length=32,null=False)
    userCode=models.CharField(max_length=64,null=False)
    creat_time = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return "".format(self.movieId, self.movieUrl)
    __str__ = __repr__


class MovieLabel(models.Model):
    class Meta:
        db_table = "labels"
    labelId = models.AutoField(primary_key=True)
    movieId = models.IntegerField()
    childId = models.IntegerField()
    labelTitle = models.CharField(max_length=128, null=False)
    labelContext=models.CharField(max_length=128, null=True)
    relation = models.CharField(max_length=32, null=False)
    userCode = models.CharField(max_length=64, null=False)
    creat_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    del_flag = models.BooleanField(default=0)  # 1删除，0正常

    def __repr__(self):
        return "".format(self.labelId, self.labelTitle)
    __str__ = __repr__