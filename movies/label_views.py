from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse

from movie.views import verifyShould, decode, encode
from movies.models import MovieLabel, Movies
from movies.movies_views import returnMovieList


def verifyAddLabel(request):#新增必填校验
    label = MovieLabel()
    label.movieId = verifyShould(request, 'movieId', True)
    label.labelTitle = decode(verifyShould(request, 'labelTitle', True))
    label.labelContext = decode(verifyShould(request, 'labelContext', False))
    label.relation = decode(verifyShould(request, 'relation', True))
    label.userCode = verifyShould(request, 'userCode', False)  # 必填校验
    label.childId = verifyShould(request, 'childId', True)
    return label

def verifyUpdateLabel(request):#更新必填校验
    label = MovieLabel()
    label.labelId = verifyShould(request, 'labelId', True)
    label.movieId = verifyShould(request, 'movieId', True)
    label.childId = verifyShould(request, 'childId', True)
    label.labelTitle = decode(verifyShould(request, 'labelTitle', True))
    label.labelContext = decode(verifyShould(request, 'labelContext', False))
    label.relation = decode(verifyShould(request, 'relation', True))
    label.userCode = verifyShould(request, 'userCode', False)  # 必填校验
    return label


def labelAdd(request:HttpResponse):
    if request.method=="POST":
        message =""
        label=verifyAddLabel(request)
        if ("no parameter" in model_to_dict(label).values()):
            message = "缺少参数"
        else:
            try:
                quest = MovieLabel.objects.filter(movieId=label.movieId,labelTitle=label.labelTitle, del_flag = False)
                    # mov = label.userCode, movieId = label.movieId, childId = label.childId, del_flag = False
                if quest:
                    message= "同一个视频不能有相同的标签"
                else:
                    label.del_flag = False
                    label.save()
                    data=model_to_dict(label)
            except Exception as e:
                print(e)
                return JsonResponse(data={"data": "保存失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def getLabelByChild(request:HttpResponse):
    if request.method=="GET":
        message=""
        data =[]
        childId=request.GET.get("childId","")
        if not childId:
            message = "缺少参数"
        else:
            try:
                quest = MovieLabel.objects.filter(childId=childId,del_flag=False).all().order_by("-creat_time")#查询是否有这个数据
                if not quest:
                    message = "没有任何记录"
                else:
                    for label in quest:
                        label.creat_time=label.creat_time.date()
                        data.append(model_to_dict(label,exclude="del_flag"))
            except Exception as e:
                    return JsonResponse(data={"message": "查询失败或没有结果"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def labelupdate(request:HttpResponse):
    if request.method=="POST":
        message=""
        label = verifyUpdateLabel(request)
        if ("no parameter" in model_to_dict(label).values()):
            message = "缺少参数"
        else:
            try:
                quest = MovieLabel.objects.filter(labelId=label.labelId,del_flag=False)#查询是否有这个数据
                if quest:
                    mylabel=model_to_dict(label,exclude=["labelId","creat_time","update_time"])
                    quest.update(**mylabel)
                    data = mylabel
                else:
                    message="没有这个child记录"
            except Exception as e:
                return JsonResponse(data={"data": "更新失败"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def getLabelByMovie(request:HttpResponse):
    if request.method=="GET":
        message=""
        data = []
        movieId=request.GET.get("movieId","")
        if not movieId:
            message = "缺少参数"
        else:
            try:
                pass
                quest = MovieLabel.objects.filter(movieId=movieId,del_flag=False).all().order_by("creat_time")#查询是否有这个数据
                if not quest:
                    message = "没有任何记录"
                else:
                    for label in quest:
                        data.append(model_to_dict(label, exclude="del_flag"))
            except Exception as e:
                    return JsonResponse(data={"message": "查询失败或没有结果"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})

def getMovieByLabelTitle(request:HttpResponse):
    if request.method=="GET":
        message=""
        data = []
        labelTitle=decode(request.GET.get("labelTitle",""))
        childId = request.GET.get("childId", "")
        if not labelTitle and not childId:
            message = "缺少参数"
        else:
            try:
                pass
                movieList = MovieLabel.objects.filter(labelTitle=labelTitle, childId=childId, del_flag=False).values_list("movieId",flat=True).order_by("creat_time")#查询是否有这个数据
                if not movieList:#movieList为queryset类型
                    message = "没有任何记录"
                else:
                    movieQuest=Movies.objects.filter(movieId__in=movieList,del_flag=False)
                    data=returnMovieList(movieQuest)
            except Exception as e:
                print(e)
                return JsonResponse(data={"message": "查询失败或没有结果"})
    else:
        message= "method错误"
    if message:
        return JsonResponse(data={"status": "ok", "message": message})
    else:
        return JsonResponse(data={"status": "ok", "data": data})