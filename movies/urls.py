from django.conf.urls import url

from movies.danmu_views import *
from movies.label_views import *
from movies.movies_views import *

urlpatterns = [
    url(r'^getall', movieGetAll),
    url(r'^getchild', movieGetOneChild),
    url(r'^addMovie', movieAdd),
    url(r'^delete', movieDelete),
    url(r'^getMovieDetail', movieGetDetail),
    url(r'^getPicDetail', picGetDetail),
    url(r'^getDanmu', danmuGetOneMovie),
    url(r'^addDanmu', danmuAdd),
    url(r'^addLabel', labelAdd),
    url(r'^getLabelByChild', getLabelByChild),
    url(r'^updateLabel', labelupdate),
    url(r'^getLabelByMovie', getLabelByMovie),
    url(r'^getMovieByLabelTitle', getMovieByLabelTitle),

]